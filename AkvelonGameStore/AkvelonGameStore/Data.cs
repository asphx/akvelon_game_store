﻿using GameClassLibrary;

namespace AkvelonGameStore
{
    internal class Data
    {
        public IList<Game> Games { get; } = new List<Game>();
        public IList<AddOn> CsgoAddons { get; } = new List<AddOn>();
        public IList<AddOn> DotaAddons { get; } = new List<AddOn>();
        public IList<AddOn> TboiAddons { get; } = new List<AddOn>();
        
        public void InizializeData()
        {
            //some code to fill data
            Games.Add(new Game("name","desc",CsgoAddons,DotaAddons));
        }
    }
}
