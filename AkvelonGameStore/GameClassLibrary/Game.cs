﻿
namespace GameClassLibrary
{
    public class Game : Product
    {
        internal IList<AddOn> AvailableAddOns { get; set; }

        internal IList<AddOn> BoughtAddOns { get; set; }

        public Game(string name, string description, IList<AddOn> addOns,IList<AddOn> boughtAddons) : base(name,description)
        {
            Name = name;
            Description = description;
            AvailableAddOns = addOns;
            BoughtAddOns = boughtAddons;
        }
        
    }
}
