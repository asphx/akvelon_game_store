﻿

namespace GameClassLibrary
{
    public class AddOn : Product
    {
        public AddOn(string name, string description) : base(name, description)
        {
            Name = name;
            Description = description;
        }
    }
}
