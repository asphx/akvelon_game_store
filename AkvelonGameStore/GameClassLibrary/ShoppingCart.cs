﻿
namespace GameClassLibrary
{
    public class ShoppingCart
    {
        public List<Game> Games { get; } = new List<Game>();

        public void AddGame(Game game)
        {
            Games.Add(game);
        }
        
        public void RemoveGame(Game game)
        {
            Games.Remove(game);
        }

        public void Clear()
        {
            Games.Clear();          
        }
        
    }
}
