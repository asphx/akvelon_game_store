

# Game store
It is a complex project made as a console application where you can choose(buy) games. Introduced by Dota, Counter-Strike: Global Offensive, The Binding of Isaac, and their add-ons. When choosing a particular game, the store offers its supplement as well as a description of the game. It ultimately saves your games and their add-ons into the cart and provides it to you.

## Features

- Adding games to cart
- Adding addons to games
- Checking their descriptions
- Buying games

## Entities
- Game - parent class for all games,has description, name, price fields and getDescripton method
- ShoppingCart - class for storing game entities, has arrayList field,addGame and removeGame methods
- Add-on - child class for game,also has fields like description, name, price fields and getDescripton method that are based on the choosen game

